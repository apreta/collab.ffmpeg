#!/bin/sh
./configure --enable-debug --prefix=/opt/iic --enable-shared --enable-libx264 --enable-libmp3lame --enable-libfaac --enable-gpl --enable-nonfree --extra-cflags="-I /opt/iic/include" --extra-ldflags="-L /opt/iic/lib"
make
